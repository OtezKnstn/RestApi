package com.example.restapi

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.restapi.databinding.RecycleElementBinding

class DataAdapter: RecyclerView.Adapter<DataAdapter.DataHolder>() {

    val elementList = ArrayList<MyDataItem>()
    var onItemClick: ((MyDataItem) -> Unit)? = null

    class DataHolder(item: View): RecyclerView.ViewHolder(item) {

        var title: TextView
        var id: TextView
        var card: CardView
        init {
            id = itemView.findViewById(R.id.numberOfMessage)
            title = itemView.findViewById(R.id.messageTitle)
            card = itemView.findViewById(R.id.card)

        }
        private val binding = RecycleElementBinding.bind(item)
        companion object {
            fun newTargetInstance(): DialogFragment {
                val fragment = DialogFragment()
                return fragment
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycle_element, parent, false)
        return DataHolder(view)
    }


    override fun onBindViewHolder(holder: DataHolder, position: Int) {
        holder.title.text = elementList[position].title
        holder.id.text = elementList[position].id.toString()
        holder.card.setOnClickListener{
            CurrentMessage.message = elementList[position].body
            CurrentMessage.title = elementList[position].title
            CurrentMessage.id = elementList[position].id
            CurrentMessage.userId = elementList[position].userId

            D.show(fragmnet,"customDialog")
        }


    override fun onBindViewHolder(holder: DataHolder, position: Int) {
        holder.bind(elementList[position])
    }

    override fun getItemCount(): Int {
        return elementList.size
    }

    fun addstroki(list: List<MyDataItem>) {
        elementList.clear()
        elementList.addAll(list)
        notifyDataSetChanged()
    }
