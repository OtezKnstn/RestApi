package com.example.restapi

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.restapi.databinding.FragmentDialogBinding


class DialogFragment: android.support.v4.app.DialogFragment() {

    lateinit var activityInterractor : ActivityInterractor
    private var _binding: FragmentDialogBinding? = null
    private val binding get() = _binding!!

    @SuppressLint("NewApi")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDialogBinding.inflate(inflater, container, false)
        val view = binding.root


        binding.btnConfirmChange.setOnClickListener() {
            TittleAndMessageClass.Tittle = binding.editTextTittle.text.toString()
            TittleAndMessageClass.Message = binding.editText.text.toString()

            Toast.makeText(context, "Сообщение успешно изменено", Toast.LENGTH_LONG).show()

            dismiss()
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ActivityInterractor) {
            this.activityInterractor = context
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        activityInterractor.OnFragmentClosed()
    }
}
